package com.maxbill.base.bean;

import lombok.Data;

@Data
public class ConfNode {

    private String key;
    private String val;
    private String txt;

}
