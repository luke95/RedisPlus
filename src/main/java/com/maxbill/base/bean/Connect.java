package com.maxbill.base.bean;

import lombok.Data;

/**
 * 连接操作实体
 *
 * @author MaxBill
 * @date 2019/07/06
 */
@Data
public class Connect {

    /**
     * 主键
     */
    private String id = "";

    /**
     * 说明
     */
    private String text = "";

    /**
     * 时间
     */
    private String time = "";

    /**
     * 是否集群：0单机，1集群
     */
    private String isha = "";

    /**
     * 类型：0默认，1：ssh
     */
    private String type = "";

    /**
     * SSH用户名
     */
    private String sname = "";

    /**
     * Redis地址
     */
    private String rhost = "";

    /**
     * SSH主机地址
     */
    private String shost = "";

    /**
     * Redis端口
     */
    private String rport = "";

    /**
     * SSH端口
     */
    private String sport = "";

    /**
     * Redis密码
     */
    private String rpass = "";

    /**
     * SSH密码
     */
    private String spass = "";


    /**
     * SSH登录私钥
     */
    private String spkey = "";

}
