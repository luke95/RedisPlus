package com.maxbill.util;

import java.util.UUID;

public class KeyUtil {

    /**
     * 生成32位的UUid
     */
    public static String getKey() {
        return UUID.randomUUID().toString().replace("-", "");
    }


}
